#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use lib 'lib';
use String::ChunkSplit;

# my ($opt_name, $opt_value) = @_;
my $siz = 255;
my $force = 0;
my ($out_mark, $in_mark) = qw(" ");
my $out_delim = ' ';
my $include_markings = 0;

GetOptions(
	'c|chars=i' => \$siz,
	'f|force!' => \$force,
	'm|input-mark=s' => sub { die('Invalid input string marking character') if $_[1] !~ /^\S$/; $in_mark = $_[1]; },
	'M|output-mark=s' => sub { die('Invalid output string marking character') if $_[1] !~ /^\S$/; $out_mark = $_[1]; },
	'D|output-delimiter=s' => sub { die('Invalid output string delimiter character') if $_[1] !~ /^.$/ && $_[1] ne '\n'; $out_delim = $_[1] ne '\n' ? $_[1] : "\n"; },
	'I|include-makings!' => \$include_markings,
) || exit 1;

local $/ = undef;
my $input = <STDIN>;
$input =~ s/\s+$//g;

my $cs = String::ChunkSplit->new(size => $siz, force => $force, in_mark => $in_mark, out_mark => $out_mark, out_delimiter => $out_delim, include_first_last_markings => $include_markings);
print $cs->split($input)."\n";
