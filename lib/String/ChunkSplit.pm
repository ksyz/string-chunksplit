package String::ChunkSplit;
use warnings;
use strict;

my $config;

sub new {
	my $class = shift;

	my $self = {
		size => 255,
		force => 0,
		out_mark => '"',
		in_mark => '"',
		out_delimiter => ' ',
		include_first_last_markings => 1,
		debug => 0,
		@_,
	};

	return bless $self, $class;
}

sub split {
	my $self = shift;
	my $input = join('', @_);
	$input =~ s/\s+$//g;

	my $strings = [''];
	my ($i_in, $i_out, $chrs, $n) = (0, 0, 0, 0);
	my ($S_str, $S_esc) = (0, 0);
	my $append = sub { $i_out++; $strings->[$n] .= shift; };

	for my $c (split(//, $input)) {
		print "$n:$c (STR: $S_str; ESC: $S_esc)\n" if $self->{debug};
		$strings->[$n] = '' if not defined $strings->[$n];
		$strings->[++$n] = '' if (length $strings->[$n] >= $self->{size});

		$i_in++;
		# input START, no output: ignore space and first "
		if ($S_str == 0 && $i_out == 0) {
			if ($c =~ /\s/) {
				next;
			}
			if ($c eq $self->{in_mark}) {
				$S_str = 1;
				next;
			}
			$append->($c); # start of string data without starting DELIMITER
			$S_str = 1;
		}
		# IN STRING
		elsif ($S_str) {
			# string end
			if ($c eq $self->{in_mark} && $S_esc == 0) {
				$S_str = 0;
				$n++ 
					if length($strings->[$n]) != 0 && $self->{force} == 0;
				next;
			}
			if ($c eq '\\') {
				$S_esc = 1;
				$append->($c);
				next;
			}
			if ($S_esc) {
				$S_esc = 0;
				$append->($c);
				next;
			}
			$append->($c);
		}
		# string start
		elsif ($i_out > 0 && $c eq '"') {
			$S_str = 1;
			next;
		}
	}

	unless ($self->{include_first_last_markings}) {
		return join ($self->{out_mark}.$self->{out_delimiter}.$self->{out_mark}, @$strings);
	}
	else {
		return join ($self->{out_delimiter}, map { $self->{out_mark}.$_.$self->{out_mark} } @$strings);
	}
}

1;
